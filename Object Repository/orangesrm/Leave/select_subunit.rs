<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_subunit</name>
   <tag></tag>
   <elementGuidId>46139a16-a46b-4c1e-bb67-5f44f696e983</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='leaveList_cmbSubunit']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#leaveList_cmbSubunit</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>leaveList[cmbSubunit]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>leaveList_cmbSubunit</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>valid</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
All
Ministry of Health
  Department of Medical Services
    MRRH
    Department of Traditional Medicine
      Traditional Healing Center Division
      Local Healing and Spiritual Health Division
      Menzerigpa and Zhibjuk Division (Medicine and Research)
      National Traditional Medicine Hospital
    DHO/Dzongkhag
      Paro Hospital
      Drukgyel PHC
      Hongtsho Primary Health Center
      Gyelposing Hospital
      Lobesa Sub Post
  JDWNRH
    Clinical Department
      Department of Medicine
      Department of Surgery
      Department of ENT
      Department of Anaestheology
      Department of Emergency
      Department of Peadiatric
      Department of Orthopaedic
      Department of Psychiatry
      Department of Ophthalmology
      Department of Physiotherapy
    Non Clinical Department
      Administrative &amp; Management Division
      Accounts &amp; Budget Division
      Property &amp; Procurement Division
      Hospital Infrastructure &amp; Maintainance Division
    Office of Nursing
      Wards
      Adult ICU
      Pediatric ICU
      Neonatal ICU
      Cabins
  Directorate of Services
    Finance Division
    Human Resource Division
    Information, Communications and Technology Division
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leaveList_cmbSubunit&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='leaveList_cmbSubunit']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='frmFilterLeave']/fieldset/ol/li[5]/select</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sub Unit'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee'])[1]/following::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Include Past Employees'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First'])[1]/preceding::select[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
   </webElementXpaths>
</WebElementEntity>
